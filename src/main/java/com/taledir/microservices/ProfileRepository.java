package com.taledir.microservices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Integer>{

	Profile findOneByEmail(String email);

	Profile findOneByUsername(String username);

}